import { Application, Router, Request, Response, NextFunction } from "express";
const userRouter: Router = require("./src/routes/userRoutes");
module.exports = (app: Application) => {
  app.use("/api/v1/users", userRouter);
  app.use((req: Request, res: Response, next: NextFunction) => {
    return res.status(404).send({
      success: false,
      message: "Requested resource not found!",
    });
  });
};
