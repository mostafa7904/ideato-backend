var express = require("express");
const Sentry =  require("@sentry/node");
const middlewares = require("./src/middlewares");
var router = require("./router");
const app = express();
Sentry.init({ dsn: 'https://cc0cadb6ceda44bf8ff7136bca458f1a@o397832.ingest.sentry.io/5252967' });
app.use(Sentry.Handlers.requestHandler());
require("dotenv").config();
const port: number = 3000;
middlewares(app);
router(app);
app.set("port", port);
const startServer = () => {
  app.listen(app.get("port"), () => {
    console.log(
      "Server is running on port ",
      app.get("port"),
      " in ",
      app.get("env"),
      "mode!"
    );
  });
};

module.exports = startServer;
