const mongoose = require("mongoose");

var connection: any = async () => {
  await mongoose.connect(
    "mongodb://localhost:27017/Ideato",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
    (e: any) => {
      if (e) throw e;
      console.log("Mongoose succesfuly connected!");
    }
  );
};

module.exports = connection;
