const User = require("../schemas/User");
var connection = require("../../database");
var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");
exports.register = async (data: RegisterData) => {
  try {
    await connection();
    const res = await User.findOne({
      email: data.email,
    });
    console.log(res);
    const newUser = new User({
      ...data,
      token: "",
    });
    const result = await newUser.save();
    if (result) return true;
    return false;
  } catch (e) {
    if (e.code === 11000) return false;
    console.log("There was an error in register/userModel!");
    console.error(e);
    return false;
  }
};

exports.login = async (data: LoginData) => {
  try {
    await connection();
    const res = await User.findOne({
      userName: data.userName,
    });
    if (res && bcrypt.compareSync(data.password, res.password)) {
      if (!res.token) {
        var token = await jwt.sign(
          JSON.stringify({ userName: res.userName, uid: res._id }),
          process.env.JWT_SECRET
        );
        await User.updateOne({ email: res.email }, { token });
      }
      return res.token || token;
    }
    return false;
  } catch (e) {
    console.log("There was an error in register/userModel!");
    console.error(e);
    return false;
  }
};
