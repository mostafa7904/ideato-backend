var express = require("express");
const userController = require("../controllers/usersController");
var router = express.Router();

router.post("/auth/register", userController.register);
router.post("/auth/login", userController.login);

module.exports = router;
