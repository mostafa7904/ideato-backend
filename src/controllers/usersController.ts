const userModel = require("../model/userModel");
import { Request, Response } from "express";
exports.register = async (req: Request, res: Response) => {
  try {
    const data: RegisterData = req.body;
    if (data) {
      const result: boolean = await userModel.register(data);
      if (result)
        return res
          .status(201)
          .send({ success: true, message: "User created!" });

      return res.status(401).send({
        success: false,
        message: "This acount already exists!",
      });
    } else {
      return res.status(422).send({
        success: false,
        message: "The payload doesn't match the schema!",
      });
    }
  } catch (e) {
    console.log("There was an error in register/controller!");
    console.error(e);
    return res
      .status(500)
      .send({ success: false, message: "There was a server error!" });
  }
};

exports.login = async (req: Request, res: Response) => {
  try {
    const data: LoginData = req.body;
    if (data) {
      const result: String = await userModel.login(data);
      if (result)
        return res.send({
          success: true,
          message: "Login was successful!",
          token: result,
        });
      return res
        .status(401)
        .send({
          success: false,
          message: "User with these information does not exist!",
        });
    }
  } catch (e) {
    console.log("There was an error in register/controller!");
    console.error(e);
    return res
      .status(500)
      .send({ success: false, message: "There was a server error!" });
  }
};
