var mongoose = require("mongoose");
import { SchemaType, HookNextFunction } from "mongoose";
var Schema = mongoose.Schema;
var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");

var UserSchema: SchemaType = new Schema(
  {
    fullName: { type: String, unique: false, required: true },
    userName: { type: String, unique: true, required: true },
    password: { type: String, unique: false, required: true },
    email: { type: String, unique: true, required: true },
    token: { type: String, unique: false, required: false },
  },
  { collection: "users" }
).pre("save", async function (this: RegisterData, next: HookNextFunction) {
  this.password = bcrypt.hashSync(this.password, 10);
  this.email = bcrypt.hashSync(this.email, 10);
  this.token = await jwt.sign(
    JSON.stringify({ email: this.email }),
    process.env.JWT_SECRET
  );
  next();
});
module.exports = mongoose.models.users || mongoose.model("users", UserSchema);
