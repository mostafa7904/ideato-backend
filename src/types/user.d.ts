interface RegisterData {
  fullName: String;
  userName: String;
  email: String;
  password: String;
  token?: String;
}

interface LoginData {
  email?: String;
  userName?: String;
  password: String;
}
