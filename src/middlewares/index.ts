"use strict";
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");
import { CorsOptions } from "cors";
import { Application } from "express";
const options: CorsOptions = {
  origin: ["http://localhost:8080"],
};
module.exports = (app: Application) => {
  app.use(cors(options));
  app.use(helmet());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
};
